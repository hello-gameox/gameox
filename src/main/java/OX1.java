

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author OMEN
 */

import java.util.Scanner;
import java.util.Arrays;
import java.util.InputMismatchException;

public class OX1 {

     static String[] board;
     static String turn;
     static String draw;

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        board = new String[9];
        turn = "X";
        String win = null;
        EmptyBoard();
        System.out.println("Welcome to OX game!!!");
        showtable();
        System.out.println("X turn");
        System.out.println("Please input:");
        while (win == null) {
            int input;
            try {
                input = kb.nextInt();
                if (!(input > 0 && input <= 9)) {
                    System.out.println("Please input Again!!!:");
                    continue;
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input; re-enter number:");
                continue;
            }

            if (board[input - 1].equals(String.valueOf(input))) {
                board[input - 1] = turn;
                if (turn.equals("X")) {
                    turn = "O";
                } else {
                    turn = "X";
                }
                showtable();
                win = checkWin();
            } else {
                System.out.println("Please input again:");
                continue;
            }
        }

        if (win.equalsIgnoreCase(draw)) {
            System.out.println("Draw! Please playing again");
        } else if(win.equalsIgnoreCase(win)){
            System.out.println("Player " + win + " Winner!!! ");
        }
    }

    
    public static void showtable() {
        System.out.println( " " + " 1 " + " " + " 2 "+ " " + " 3 ");
        System.out.println("1 " + board[0] + " | " + board[1] + " | " + board[2]);
        System.out.println("2 " + board[3] + " | " + board[4] + " | " + board[5]);
        System.out.println("3 " + board[6] + " | " + board[7] + " | " + board[8]);
    }

    public static void EmptyBoard() {
        for (int a = 0; a < 9; a++) {
            board[a] = String.valueOf(a + 1);
        }
    }
    
    public static String checkWin() {
        for (int a = 0; a < 8; a++) {
            String row = null;
            switch (a) {
               case 0:
                row = board[0] + board[1] + board[2];
                break;
               case 1:
                row = board[3] + board[4] + board[5];
                break;
               case 2:
                row = board[6] + board[7] + board[8];
                break;
               case 3:
                row = board[0] + board[3] + board[6];
                break;
               case 4:
                row = board[1] + board[4] + board[7];
                break;
               case 5:
                row = board[2] + board[5] + board[8];
                break;
               case 6:
                row = board[0] + board[4] + board[8];
                break;
               case 7:
                row = board[2] + board[4] + board[6];
                break;
            }
            if(row.equals("XXX")){
                return "X";
            }else if(row.equals("OOO")){
                return "O";
            }
        }
            for (int a = 0; a < 8; a++) {
                
                if(a == 8){
                    return "draw";
                }
        }
        System.out.println(turn + " turn");
        System.out.println("Please Input:");
        return null;
    }
}
